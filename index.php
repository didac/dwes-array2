<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $table = newTable(4);
        viewTable($table);
        viewTable(viewTableRever($table));

        function newTable($n) {
            $taula = array();
            for ($i = 0; $i <= $n; $i++) {
                for ($j = 0; $j <= $n; $j++) {
                    if ($i == $j) {
                        $taula[$i][$j] = "*";
                    } else if ($i > $j) {
                        $taula[$i][$j] = $i + $j;
                    } else {
                        $taula[$i][$j] = rand(10, 20);
                    }
                }
            }
            return $taula;
        }

        function viewTable($taula) {
            print_r("<table>");
            for ($i = 0; $i <= count($taula); $i++) {
                print_r("<tr>");
                for ($j = 0; $j <= count($taula); $j++) {
                    print_r("<td>" . $taula[$i][$j] . "</td>");
                }
                print_r("</tr>");
            }
        }

        function viewTableRever($taula) {
            $taulaRever = array();
            for ($i = 0; $i <= count($taula); $i++) {
                for ($j = 0; $j <= count($taula); $j++) {
                    $taulaRever[$j][$i] = $taula[$i][$j];
                }
                return $taulaRever;
            }
        }
        ?>
    </body>
</html>
